<?php

/**
 * This page uses tags to only invalidate result pages that contain the concerned content.
 *
 * curl -sD - varnish.lo/exercises/tagging/?filter=apple
 * curl -sD - varnish.lo/exercises/tagging/?filter=orange
 * curl -X POST --data 'applepie=New description' -sD - varnish.lo/exercises/tagging/update.php
 * curl -sD - varnish.lo/exercises/tagging/?filter=apple
 * # this is still a cache hit
 * curl -sD - varnish.lo/exercises/tagging/?filter=orange
 */

require __DIR__.'/init.php';

if (isset($_GET['filter'])) {
    foreach ($items as $key => $value) {
        if (false === strpos($key, $_GET['filter'])) {
            unset($items[$key]);
        }
    }
}

header('Cache-Control: public, s-maxage=3600');
header('X-Cache-Tags: ' . implode(',', array_keys($items)));

echo date("Y-m-d H:i:s") . "\n";
foreach ($items as $key => $item) {
    echo "$key: $item\n";
}
