<?php
session_start();
$role = $_SESSION['role'] ? $_SESSION['role'] : 'anonymous';

header('Vary: X-User-Context-Hash');
header('Cache-Control: max-age=0');
header('X-Reverse-Proxy-TTL: 3600');

if (in_array($role, array('admin', 'user'))) {
    $body = '<h1>Hi logged in user...</h1>';
} else {
    header('HTTP/1.1 403');
    $body = '<h1>You need to log in first</h1>';
}

require __DIR__.'/template.php';
