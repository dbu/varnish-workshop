<?php

// A POST to this page should invalidate it

require __DIR__.'/../../vendor/autoload.php';

use FOS\HttpCache\ProxyClient\HttpDispatcher;
use FOS\HttpCache\ProxyClient\Varnish;

header('Cache-Control: s-maxage=300');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // NOTE: the port is different here because the tests launch their own varnish on port 6181
    $varnish = new Varnish(new HttpDispatcher(['http://127.0.0.1:6181'], 'localhost:6181'));

    // something is missing here
}

echo date("Y-m-d H:i:s") . "\n";
