<?php
require __DIR__.'/../../../vendor/autoload.php';
require __DIR__.'/init.php';

use FOS\HttpCache\CacheInvalidator;
use FOS\HttpCache\ProxyClient\HttpDispatcher;
use FOS\HttpCache\ProxyClient\Varnish;

if ('POST' !== $_SERVER['REQUEST_METHOD']) {
    die('Go to ./');
}

$toInvalidate = [];
foreach (array_keys($items) as $key) {
    if (isset($_POST[$key])) {
        $items[$key] = $_POST[$key];
        // TODO: keep track of the keys
    }
}

file_put_contents(DATAFILE, serialize($items));

if (count($toInvalidate)) {
    $client = new Varnish(new HttpDispatcher(['127.0.0.1'], 'varnish.lo'));
    $invalidator = new CacheInvalidator($client);

    // TODO: instead of invalidating all filter URLs, use cache tags
    $invalidator
        ->invalidateRegex('^/exercises/tagging/.*')
        ->flush()
    ;
}

header('Location: http://varnish.lo' . dirname($_SERVER['PHP_SELF']));

