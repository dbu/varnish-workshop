<?php
session_start();
$role = $_SESSION['role'] ? $_SESSION['role'] : 'anonymous';

header('Vary: X-User-Context-Hash');
header('Cache-Control: max-age=0');
header('X-Reverse-Proxy-TTL: 3600');
$body = '<h1>User Context Demo</h1>';
if ('anonymous' === $role) {
    $body .= '<p>Please <a href="login.php">Log in</a>.</p>';
}

require __DIR__.'/template.php';
