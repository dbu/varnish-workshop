<?php

const DATAFILE = '/tmp/tagging';

if (file_exists(DATAFILE)) {
    $items = unserialize(file_get_contents(DATAFILE));
} else {
    $items = array(
        'applepie' => 'A delicious applie pie',
        'applejuice' => 'Juicy fruit juice',
        'orangejuice' => 'Another flavour of juice',
    );
}
