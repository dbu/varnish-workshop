<?php
session_start();

if ('POST' === $_SERVER['REQUEST_METHOD']) {
    $_SESSION['demo'] = $_POST['demo'];

    header('Location: http://varnish.lo' . $_SERVER['PHP_SELF']);
    die;
}
?>

<html>
    <head><title>Cookie demo</title></head>
    <body>
        <form method="post">
            <input type="text" name="demo" value="<?php echo $_SESSION['demo']; ?>"/>
            <input type="submit"/>
        </form>
        <a href="./">Back to static page</a>
    </body>
</html>
