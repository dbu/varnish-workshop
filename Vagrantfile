# -*- mode: ruby -*-
# vi: set ft=ruby :

##############################################################################
#                    setup your site's configuration                         #
##############################################################################

# the ip address where the vm can be accessed from the host
vm_ip                   = "10.11.12.42"
host_name               = "varnish.lo"
vagrant_config_folder   = "/vagrant"

host_aliases = %w(varnish.lo)

def Kernel.is_windows?
    # Detect if we are running on Windows
    processor, platform, *rest = RUBY_PLATFORM.split("-")
    platform == 'mingw32'
end

Vagrant.configure("2") do |config|
    config.vm.box = "bento/debian-7.8"
    config.vm.box_url = "chef/debian-7.8"
    config.vm.box_version = "= 2.2.1"
    config.vm.synced_folder ".", "/vagrant"
    config.vm.network :private_network, ip: vm_ip
    config.vm.hostname = host_name

    # Speeds up connections (like composer)
    config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        vb.memory = 1024
    end

    if Vagrant.has_plugin?("vagrant-hostmanager")
      config.hostmanager.enabled = true
      config.hostmanager.manage_host = true
      config.hostmanager.ignore_private_ip = false
      config.hostmanager.aliases = host_aliases
    end
    if Vagrant.has_plugin?("vagrant-hostsupdater")
      config.hostsupdater.aliases = host_aliases
    end

    config.vm.provision "shell",
        inline: "apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -y install chef"

    config.vm.provision :chef_solo do |chef|

        chef.cookbooks_path = "./"
        # chef debug level, start vagrant like this to debug:
        # $ CHEF_LOG_LEVEL=debug vagrant <provision or up>
        chef.log_level = ENV['CHEF_LOG'] || "info"

        # chef recipes/roles
        chef.add_recipe("vagrant")

        host_ip = vm_ip[/(.*\.)\d+$/, 1] + "1"
        chef.json = {
          :refreshdb => true,
          :host_ip => host_ip,
          :host_name => host_name,
          :host_aliases => host_aliases,
        }
    end

    # the system tries to boot varnish at a point when the /vagrant folder is not yet mounted
    # varnish does not start because it sees no configuration files
    # http://razius.com/articles/launching-services-after-vagrant-mount/

    if Vagrant::VERSION =~ /^1.[0-5]/
        puts "Except on initial provision, you will need to start varnish manually, as your vagrant version is too old to handle it automatically"
    else
        config.vm.provision :shell, run: "always", inline: "sudo service varnish restart"
    end
end

if Vagrant::VERSION =~ /^1.[0-5]/
    puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    puts "!!! Run 'sudo service varnish restart' inside the box to get varnish running (or use vagrant >= 1.6 to have it automatically) !!!"
    puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
end
